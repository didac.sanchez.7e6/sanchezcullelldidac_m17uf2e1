using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.PlayerSettings;

public class CanonRotation : MonoBehaviour
{
    public Vector3 _maxRotation;
    public Vector3 _minRotation;
    private float offset = -51.6f;
    public GameObject ShootPoint;
    public GameObject Bullet;
    public float ProjectileSpeed = 0;
    public float MaxSpeed;
    public float MinSpeed;
    public GameObject PotencyBar;
    private float initialScaleX;

    private void Awake()
    {
        initialScaleX = PotencyBar.transform.localScale.x;
    }
    void Update()
    {
        var mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);//guardem posici� de la c�mera
        Vector3 posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
        var dist = mousePos - posOnScreen;//dist�ncia entre el click i la bala
        var ang = (Mathf.Atan2(dist.y, dist.x) * 180f / Mathf.PI + offset);
        transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, ang));//angle que s'ha de rotar
        Debug.Log($"rat{mousePos}, ang {ang}, rotation {ShootPoint.transform.rotation}");
        if (Input.GetMouseButton(0))
        {
            ProjectileSpeed += 4;//cada frame s'ha de fer 4 cops m�s gran
        }
        if (Input.GetMouseButtonUp(0))
        {
            var projectile = Instantiate(Bullet, ShootPoint.transform.position, Quaternion.identity);//On s'instancia?
            projectile.GetComponent<Rigidbody2D>().velocity = new Vector2(ProjectileSpeed / MaxSpeed * Mathf.Sin(ang), ProjectileSpeed / MaxSpeed * Mathf.Cos(ang));//quina velocitat ha de tenir la bala? s'ha de fer alguna cosa al vector direcci�?
            ProjectileSpeed = 0f;
        }
        CalculateBarScale();

    }
    public void CalculateBarScale()
    {
        PotencyBar.transform.localScale = new Vector3(Mathf.Lerp(0, initialScaleX, ProjectileSpeed / MaxSpeed),
            transform.localScale.y,
            transform.localScale.z);
    }
}
